from common import *
import re


class Solutions():
    ''' This class is for solutions end point expected response'''

    def __init__(self,):
        self.common=Common()
        self.solution_urls=solution_urls('solutionManager')
        

    def get_solutions(self, qr_prms, solutions):
        '''This function will give expected response of solution endpoint based on query parameters 
           
            parameters:
                       qr_prms   --> str
                       solutions --> tuple
        '''
        
        offset = 0
        solution_ids = []
        installed_solutions = len(list(solutions))
        limit = installed_solutions
        include_member = False
        selected_solutionId_count=0

        # Convering query parameters to dict type and content filters to list type
        qry_prm, content_Filters = common.get_query_params(qr_prms)

        # Assigning values to query parameters based on input else default values
        if qry_prm != None:
            offset = int(qry_prm.get('offset', 0))
            limit = int(qry_prm.get('limit', installed_solutions))
            include_member = bool(qry_prm.get("include_member", False))
            

        # Getting solution_ids based on offset and limit values
        solution_ids = common.get_solutionIds(
            offset, limit, solutions, installed_solutions)

        # Getting solution_id  based on provided query parameter from user
        
        if 'select_solutionId' in qry_prm:
            select_solutionId = str(qry_prm.get('select_solutionId'))
            if select_solutionId in solution_ids:
                solution_ids = select_solutionId
            else:
                BadParameters(Statuscode.client_error,
                              "Solution_id is not valid").error_response()

        expected_response = {
            "links": [
                {
                    "href":  self.solution_urls.urls(),
                    "rel": "self"
                },
                {
                    "hrefTemplate": self.solution_urls.urls()+"{solutionId}}",
                    "rel": "solution"
                }
            ],
            "memberIds": solution_ids,
            "installed_solutions": installed_solutions,
            "offset": offset

        }

        # providing members data to expected response
        if include_member == True:
            members = common.get_members(offset, limit, solutions)
            expected_response['members'] = members

        # providing only content filter data to expected response
        expected_response=common.get_contentFilterData(expected_response,qry_prm,content_Filters)
        

        if "links" in expected_response:
           error = common.links_comparision(
               expected_response['links'], Response.actual_response["links"])
           if len(error) != 0:
               print(error)
        
        
        # Generating json file to show output
        result = json.dumps(expected_response, indent=4)
        out_file = open("output/test.json", "w")
        out_file.write(result)
        out_file.close()



solutionsmanager = Solutions()

common = Common()

solutions = common.get_manifests()

solutions = tuple(solutions)


# Reading query parameters from user
qr_prms = input("Enter  query parameters for solutions endpoint :")


solutionsmanager.get_solutions(qr_prms, solutions)
