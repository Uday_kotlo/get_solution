from requests.packages.urllib3.fields import RequestField
from requests.packages.urllib3.filepost import encode_multipart_formdata,iter_fields
import json
import urllib3




content={'app_version':1234}

context={'data':'session_context'}

with open("unverifiedBundle2.bdl", encoding="utf8", errors='ignore') as binary:
                bundle_file = binary.read()
               


class common():
        ''' This is a common class to raise post multipart/mixed request'''

        url='http://httpbin.org/post'

        def __init__(self):
            
            '''The constructor for common class'''

            self.postrequest = postrequest()
            self.MULTIPART_MIXED = 'multipart/mixed'
            self.MULTIPART_FORMDATA = 'multipart/formdata'
            
       
        
        def post_multiparmixed_request(self,content,context,bundle_file,content_type=None):
            '''This Function is to raise multipart/mixed request
               
               Args:
                    content: json object
                    context: json object
                    bundle_file: binary data
                    content_type: content_type for POST request

                returns:
                    response object
              
              '''
            headers={}

            fields={'content':('content',json.dumps(content),'application/json'),
                    'context':('context',json.dumps(content),'application/json'),
                    'manifest':('manifestfile',  bundle_file,'solution.bdl')
                    }


            if content_type==None:
                content_type=self.MULTIPART_MIXED

            post_body,content=self.postrequest.post_multiparmixed_binarydata(fields,content_type)
            
            boundary=content.split('=')[-1]
            
            headers['Content-Type']=content_type
            headers['boundary']=boundary
            headers['Content-Disposition']='attachment'
            
        
            response=self.postrequest.post_multiparmixed(common.url, post_body, headers)
           
            print(response.status)
            print(response._body)





class postrequest():


    def post_multiparmixed_binarydata(self,fields,content_type):
            ''' This function will give binary data of fields
             
                Args:
                    fields: dict of field
                    content_type:By default value is multipart/mixed

                returns:
                    returns binary data of fields and content type
            '''
            fields_type=[]

            for name,(fname,contents,content) in fields.items():
                field=RequestField(name=name, data=contents, filename=fname)
                field.make_multipart(content_type=content)
                fields_type.append(field)
            
            post_body,content = encode_multipart_formdata(fields_type)

            return post_body,content

    def post_multiparmixed(self,url,post_body,headers):
            ''' This function is to send post request
            
                Args:
                   url: The API url
                   post_body: binary data
                   headers: dict of headers

                returns:
                   returns response object
            '''
            http = urllib3.PoolManager()
            response=http.request('POST',url,body=post_body,headers=headers)
            return response


if __name__=='__main__':

    
    common=common()
    # postrequest=common.postrequest
    common.post_multiparmixed_request(content,context,bundle_file)