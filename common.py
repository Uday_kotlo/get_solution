import pathlib
import sys
import json

class BadParameters():
    '''This class is for generating bad parameters json file
       
       parameters:
                  code --> int
                  message --> str
    '''


    def __init__(self,code,message):
        self.code=code
        self.message=message

    def error_response(self):
        '''This funtion is for generating bad parameter response in json file '''

        errors={
             "errors":{
                          
                          "message":self.message,
                          "httpStatusCode":self.code
                      }
               }
        result = json.dumps(errors, indent=4)
        out_file = open("output/test.json", "w")
        out_file.write(result)
        out_file.close()
        sys.exit()


class Common:
    '''This class has the common functions for the solution end point'''
    
    def get_manifests(self):
        '''Adding manifest files into generator
           Reading files from current directory and returning generator
        '''
        for file in pathlib.Path('.').iterdir():
           if file.is_file and file.name.endswith("json"):
               with open(file) as fil:
                   data=json.load(fil)
                   yield data

    def get_solutionIds(self,offset, limit, solutions, installed_solutions):
            '''This function returning list of solution_ids
               
               Parameters:
                         offset --> int
                         limit --> int
                         solutions --> tuple
                         installed_solutions --> int
            '''
    
            solution_ids = []
            if offset < 0 or limit <= 0:
               BadParameters(Statuscode.client_error,"query parameter has negitive value or zero value").error_response()

            elif offset > installed_solutions:
                solution_ids = []

            else:
                 limit = limit if limit != 0 else installed_solutions
                 for i in range(offset, offset+limit):
                    if i in range(len(list(solutions))):
                        solution_ids.append(solutions[i]["solutionId"])
                 return solution_ids

    def get_members(self,offset, limit,manifests):
            '''This function is returning list of members data
              
               parameters:
                         offset --> int
                         limit --> int
                         manifests --> tuple             
            '''

            keys_in_members = ["solutionId", "contextProfile",
                       "description", "registrations"]
            members_data = []
            for manifest in manifests[offset:offset+limit]:
                 empty_member_data = {}
                 for key in keys_in_members:
                        empty_member_data[key] = manifest[key]
                 members_data.append(empty_member_data)
            members = members_data
            return members
    
    def links_comparision(self,actual,expected):
        ''' This function returns list of errors 
            
            parameters:
                      actual-->list
                      expected-->list
        '''
        error=[]
        if len(actual)!=len(expected):
            message="The length of actual and expected response are not same"
            error.append(message)
            return error
        else:
            for link in actual:
                if link not in expected:
                    message='{0} is not same in expected response'.format(link)
                    error.append(message)
            for link in expected:
                if link not in actual:
                    message='{0} is not same in actual response'.format(link)
                    error.append(message)
        return error

    def get_contentFilterData(self,expected_response,qry_prm,content_Filters):
            ''' This function returns expected response in dict type
                 
                parameters:
                         expected_response --> dict
                         qry_prm --> dict
                         content_Filters --> list

            '''

            if 'contentFilter' in qry_prm:
                expected_response_Filters = {}

                for filter in content_Filters:
                    expected_response_Filters[filter] = expected_response[filter]
                expected_response = expected_response_Filters
            return expected_response

    def get_query_params(self,qry_prm):
            '''This function returns qyery_parameters in dict type and content_filters in list type

               parameters:
                          qry_prm -->str
            '''

            qr_prms = dict()
            contentFilter = ""
            
            if  ("?" and "=") in qry_prm:
                 try:
                     qry_prm = qry_prm.split("?")
                     qry_prm = "".join(qry_prm[1])
                     qry_prm = qry_prm.split("&")
                     for item in qry_prm:
                         key, value = (item.split("="))
                         qr_prms[key] = value
                 except:
                   BadParameters(Statuscode.client_error,"provided query parameter is not valid").error_response()
            
            else:
                 if len(qry_prm)!= 0:
                       BadParameters(Statuscode.client_error,"query parameters are not valid").error_response()

            # If content filter in query parameters then we are converting them to list type
            if "contentFilter" in qr_prms:
                 contentFilter = qr_prms['contentFilter']
                 contentFilter = contentFilter.replace("[", "")
                 contentFilter = contentFilter.replace("]", "")
                 contentFilter = contentFilter.replace("'", "")
                 contentFilter = contentFilter.split(",")

            return qr_prms, contentFilter


class Statuscode():
    ''' This class have variables for http status codes'''

    success=200
    redirection=300
    client_error=400
    server_error=500

class solution_urls():

    def __init__(self,service):
        self.service=service
    
    def urls(self):
        base="/ext"
        version="v1"
        service="solutionManager"
        url='{0}/{1}/{2}/{3}'.format(base,self.service,version,'solutions')
        return url

class Response:
    '''This class have the actual_response variable we will use it for comparision'''

    actual_response={
    "links": [
        {
            "href": "/ext/solutionManager/v1/solutions",
            "rel": "self"
        },
        {
            "hrefTemplate": "/ext/solutionManager/v1/solutions{solutionId}}",
            "rel": "solution"
        }
    ],
    "memberIds": "ce3013f3-f8c8-440a-b63a-47c205fdff66",
    "installed_solutions": 4,
    "offset": 0
      } 
      

Response=Response()

